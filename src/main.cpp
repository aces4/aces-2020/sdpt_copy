#include <iostream>

/**
 * Program entry point, it will display a cute message.
 * @param argc The number of arguments provided when the program was started.
 * @param argv The argument array (argv[0] is usually the executable name).
 * @return an error code, where 0 means the execution was successful.
 */
int main(int argc, char ** argv) {
	std::cout << "Hello world of Git!" << std::endl;
	return 0;
}
